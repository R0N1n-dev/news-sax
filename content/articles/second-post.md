---
title: Second post
description: Stories of the last Ronin for the Island
img: onions.jpg
author:
  name: Ronin
  bio: A trained nutritionist and self-taught front-end web devloper
  
---

<info-box>
  <template #info-box>
    This is a vue component inside markdown using slots
  </template>
</info-box>

[^bignote]: Here's one with multiple paragraphs and code.

    Indent paragraphs to include them in the footnote.

    `{ my code }`

    Add as many paragraphs as you like.